//
//  ServicesListCell.swift
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit
import Kingfisher

class ServicesListCell: UICollectionViewCell {
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textCost: UILabel!
    
    func update(service:Service) {
        titleLabel.text = service.title
        if let media = service.media, let address = media.address {
            let url = URL(string: AppDelegate.info["ServerAddress"] as! String + address)
            imageIcon.kf.setImage(with: url)
        }
        textCost.text = "Cost: " + String(format:"%.2f",service.cost!) + "$"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
