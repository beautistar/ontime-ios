//
//  MainViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import PopupDialog
import LGButton
import Whisper
import Tabman
import MarqueeLabel
import RMPickerViewController

class MainViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var map:GMSMapView!
    var markerPickup = GMSMarker()
    var markerDropOff = GMSMarker()
    var pickUpLocation = CLLocationCoordinate2D()
    var destinationLocation = CLLocationCoordinate2D()
    var currentLocation = CLLocationCoordinate2D()
    var arrayDriversMarkers: [GMSMarker] = []
    enum MarkerMode {
        case origin, destination, services
    }
    var markerMode = MarkerMode.origin
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var locationManager = CLLocationManager()
    var servicesViewController: ServicesParentViewController?
    
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var buttonConfirmLocation: LGButton!
    @IBOutlet weak var containerServices: UIView!
    @IBOutlet weak var locationLabel: MarqueeLabel!
    @IBOutlet weak var imageMarker: UIImageView!
    @IBOutlet weak var leftBarButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        map.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.onStartTravelRequested), name: .startTravel, object: nil)
        drawSearchBarShadow()
        let tap = UITapGestureRecognizer(target: self, action: #selector(MainViewController.tapLocation))
        locationLabel.addGestureRecognizer(tap)
    }
    
    @objc func tapLocation(sender:UITapGestureRecognizer) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.isTranslucent = true
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    @IBAction func onButtonConfirmLocationClicked() {
        if markerMode == .origin {
            buttonConfirmLocation.titleString = "CONFIRM DESTINATION"
            markerMode = .destination
            leftBarButton.setImage(#imageLiteral(resourceName: "back"),for: .normal)
            pickUpLocation = map.camera.target
            Travel.shared.pickupPoint = pickUpLocation
            imageMarker.image = #imageLiteral(resourceName: "marker destination")
            if markerPickup.map == nil {
                markerPickup.map = map
                markerPickup.icon = #imageLiteral(resourceName: "marker pickup")
            }
            markerPickup.position = pickUpLocation
            let cameraTarget = CLLocationCoordinate2D(latitude: pickUpLocation.latitude + 0.0015, longitude: pickUpLocation.longitude)
            map.animate(toLocation: cameraTarget)
        } else {
            markerMode = .services
            destinationLocation = map.camera.target
            markerDropOff.position = destinationLocation
            Travel.shared.destinationPoint = destinationLocation
            imageMarker.isHidden = true
            if markerDropOff.map == nil {
                markerDropOff.map = map
                markerDropOff.icon = #imageLiteral(resourceName: "marker destination")
            }
            markerDropOff.position = destinationLocation
            map.padding = UIEdgeInsets(top: 100, left: 0, bottom: 200, right: 0)
            var bounds = GMSCoordinateBounds()
            bounds = bounds.includingCoordinate(pickUpLocation)
            bounds = bounds.includingCoordinate(destinationLocation)
            let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
            map.animate(with: update)
            map.settings.setAllGesturesEnabled(false)
            RiderSocketManager.shared.calculateFare(pickupLocation: pickUpLocation, destinationLocation: destinationLocation) { result in
                let result = result as CalculateFareResultEvent
                if(result.hasError()){
                    result.errorWhisper(view: self.navigationController!)
                    return
                }
                self.containerServices.isHidden = false
                self.servicesViewController?.reloadPages()
            }
        }
    }
    
    func goBackFromServiceSelection() {
        leftBarButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        map.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        markerPickup.map = nil
        markerDropOff.map = nil
        self.containerServices.isHidden = true
        self.imageMarker.isHidden = false
        markerMode = .origin
        map.settings.setAllGesturesEnabled(true)
        map.animate(toLocation: pickUpLocation)
        imageMarker.image = #imageLiteral(resourceName: "marker pickup")
        buttonConfirmLocation.titleString = "CONFIRM PICKUP"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ServicesParentViewController,
            segue.identifier == "segueServices" {
            self.servicesViewController = vc
        }
    }
    
    func drawSearchBarShadow(){
        searchBarView.layer.shadowColor = UIColor.black.cgColor
        searchBarView.layer.shadowRadius = 8
        searchBarView.layer.shadowOpacity = 0.2
        searchBarView.layer.shouldRasterize = true
    }
    
    @IBAction func onRequestRideClicked(_ sender: LGButton) {
        
        let ratingVC = RequestDialogViewController(nibName: "RequestDialogViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true) {
            if ratingVC.dialogResult != nil && ratingVC.dialogResult.hasError() {
                ratingVC.dialogResult.errorWhisper(view: self.navigationController!)
                return
            }
            if ratingVC.dialogResult != nil && ratingVC.dialogResult.response == .OK {
                self.performSegue(withIdentifier: "startTravel", sender: nil)
            }
        }
        
        present(popup, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let position = manager.location else {
            let popup = DialogBuilder.getDialogForMessage(message: "Couldn't get current location. use search to find your current place on map or make sure GPS is enabled on your device.")
            self.present(popup, animated: true, completion: nil)
            locationManager.stopUpdatingLocation()
            return
        }
        map.animate(to: GMSCameraPosition.camera(withTarget: position.coordinate, zoom: 16.0))
        currentLocation = position.coordinate
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func onMenuClicked(_ sender: UIButton) {
        switch markerMode {
        case .origin:
            NotificationCenter.default.post(name: .menuClicked, object: nil)
            break
        case .destination:
            leftBarButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
            imageMarker.image = #imageLiteral(resourceName: "marker pickup")
            map.animate(toLocation: pickUpLocation)
            markerPickup.map = nil
            markerMode = .origin
            buttonConfirmLocation.titleString = "CONFIRM PICKUP"
            break
        case .services:
            goBackFromServiceSelection()
        }
    }
    
    @objc func onStartTravelRequested(notification:Notification){
        let ratingVC = RequestDialogViewController(nibName: "RequestDialogViewController", bundle: nil)
        ratingVC.selectedServiceId = notification.object as? Int
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true) {
            if ratingVC.dialogResult != nil && ratingVC.dialogResult.hasError() {
                ratingVC.dialogResult.errorWhisper(view: self.navigationController!)
                return
            }
            if ratingVC.dialogResult != nil && ratingVC.dialogResult.response == .OK {
                self.performSegue(withIdentifier: "startTravel", sender: nil)
            }
        }
        
        present(popup, animated: true)
    }
    
    @IBAction func onFavoritesClicked(_ sender: UIButton) {
        RiderSocketManager.shared.crudAddress(crud: CRUD.READ, address: Address()) { response,addresses in
            Address.lastDownloaded = addresses
            let style = RMActionControllerStyle.white
            let selectAction = RMAction<UIPickerView>(title: "Select", style: .done) { controller in
                self.map.animate(toLocation: addresses[controller.contentView.selectedRow(inComponent: 0)].location!)
            }
            
            let cancelAction = RMAction<UIPickerView>(title: "Cancel", style: .cancel) { _ in
                print("Row selection was canceled")
            }
            
            let actionController = RMPickerViewController(style: style, title: "Favorite Addresses", message: "Choose the location from picker in below then use Select button.", select: selectAction, andCancel: cancelAction)!;
            
            actionController.picker.delegate = self;
            actionController.picker.dataSource = self;
            
            if actionController.responds(to: Selector(("popoverPresentationController:"))) && UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
                actionController.modalPresentationStyle = UIModalPresentationStyle.popover
            }
            self.present(actionController, animated: true, completion: nil)
        }
        
    }
    @IBAction func onCurrentLocationClicked(_ sender: UIButton) {
        map.animate(to: GMSCameraPosition.camera(withTarget: currentLocation, zoom: 16.0))
    }
    
    func getAddressForLatLng(location: CLLocationCoordinate2D) -> String {
        let baseUrl = "https://maps.googleapis.com/maps/api/geocode/json?"
        let apikey = "AIzaSyA2TvLxS-8nI-hl6tJcaG0wwyW2nA7wdic"
        let url = NSURL(string: "\(baseUrl)latlng=\(location.latitude),\(location.longitude)&key=\(apikey)")
        let data = NSData(contentsOf: url! as URL)
        let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
        if let dictionary = json as? [String: Any] {
            if let results = dictionary["results"] as? [Any] {
                if let firstObject = results.first as? [String:Any] {
                    if let formattedAddress = firstObject["formatted_address"] as? String {
                        return formattedAddress
                    }
                }
            }
        }
        return "Unknown Address"
    }
    
    func getLocationAsync(location: CLLocationCoordinate2D){
        DispatchQueue.global(qos: .background).async {
            let res = self.getAddressForLatLng(location: location)
            
            DispatchQueue.main.async {
                self.locationLabel.text = res
                if self.markerMode == .origin{
                    Travel.shared.pickupAddress = res
                } else {
                    Travel.shared.destinationAddress = res
                }
            }
        }
    }
}

extension MainViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: {
            self.map.animate(toLocation: place.coordinate)
        })
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
extension MainViewController:GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        getLocationAsync(location: position.target)
        RiderSocketManager.shared.getDriversLocation(point: mapView.camera.target) { serverResponse,result  in
            for driverMarker in self.arrayDriversMarkers {
                driverMarker.map = nil
            }
            self.arrayDriversMarkers.removeAll()
            for location in result {
                let marker = GMSMarker(position: location)
                marker.icon = #imageLiteral(resourceName: "marker taxi")
                marker.map = self.map
                self.arrayDriversMarkers.append(marker)
            }
        }
        if markerMode == .origin{
            pickUpLocation = position.target
            Travel.shared.pickupPoint = pickUpLocation
        } else {
            destinationLocation = position.target
            Travel.shared.destinationPoint = destinationLocation
        }
    }
}

extension MainViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    // MARK: UIPickerView Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Address.lastDownloaded.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Address.lastDownloaded[row].title;
    }
}
