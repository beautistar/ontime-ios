//
//  NavigationMenuViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import InteractiveSideMenu
import Kingfisher
import KingfisherWebP

class NavigationMenuViewController : MenuViewController {
    let kCellReuseIdentifier = "MenuCell"
    let menuItems = ["Main"]
    
    @IBOutlet weak var imageUser: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelCredit: UILabel!
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageUser.layer.cornerRadius = imageUser.frame.size.height / 2
        imageUser.clipsToBounds = true
        imageUser.layer.borderColor = UIColor.white.cgColor
        imageUser.layer.borderWidth = 3.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let riderImage = AppConfig.shared.user?.media?.address {
            let processor = (WebPProcessor.default >> ResizingImageProcessor(referenceSize: CGSize(width: 100, height: 100), mode: .none)) >> RoundCornerImageProcessor(cornerRadius: 50)
            imageUser.kf.setImage(with: URL(string: AppDelegate.info["ServerAddress"] as! String + riderImage), placeholder: nil, options: [.processor(processor)])
        }
        if let firstName = AppConfig.shared.user?.firstName, let lastName = AppConfig.shared.user?.lastName {
            labelName.text = firstName + " " + lastName
        }
        if let balance = AppConfig.shared.user?.balance {
            labelCredit.text = String(format: "Balance: %.2f $", balance)
        }
    }
    
    @IBAction func onTravelsClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showTravels", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onProfileClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showEditProfile", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onWalletClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showWallet", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onFavoritesClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showFavorites", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onAboutClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showAbout", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onExitClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        if let bundle = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundle)
            self.dismiss(animated: true, completion: nil)
            menuContainerViewController.hideSideMenu()
        }
    }
}
