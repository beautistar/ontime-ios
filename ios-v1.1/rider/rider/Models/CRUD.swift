//
//  CRUD.swift
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit

enum CRUD:Int {
    case CREATE = 0
    case READ = 1
    case UPDATE = 2
    case DELETE = 3
    /**
     * The name of the enumeration (as written in case).
     */
    var name: String {
        get { return String(describing: self) }
    }
    
    /**
     * The full name of the enumeration
     * (the name of the enum plus dot plus the name as written in case).
     */
    var description: String {
        get { return String(reflecting: self) }
    }
}
