//
//  Address.swift
//
//  Copyright (c) Minimalistic apps. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

public final class Address: Mappable {
    
    public static var lastDownloaded = [Address]()
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let title = "title"
        static let location = "location"
        static let id = "id"
        static let riderId = "rider_id"
        static let address = "address"
    }
    
    // MARK: Properties
    public var title: String?
    public var location: CLLocationCoordinate2D?
    public var id: Int?
    public var riderId: Int?
    public var address: String?
    
    init() {
        
    }
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        title <- map[SerializationKeys.title]
        location <- (map[SerializationKeys.location],LocationXYTransform())
        id <- map[SerializationKeys.id]
        riderId <- map[SerializationKeys.riderId]
        address <- map[SerializationKeys.address]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = location { dictionary[SerializationKeys.location] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = riderId { dictionary[SerializationKeys.riderId] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        return dictionary
    }
    
}
