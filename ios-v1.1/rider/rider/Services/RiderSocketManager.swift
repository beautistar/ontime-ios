//
//  RiderSocketManager.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation
import SocketIO
import ObjectMapper
import GoogleMaps

class RiderSocketManager{
    var socket : SocketIOClient
    var manager: SocketManager
    static let shared = RiderSocketManager()
    init() {
        manager = SocketManager(socketURL: URL(string: AppDelegate.info["ServerAddress"] as! String)!)
        socket = manager.defaultSocket
    }
    
    func connect(token:String,completionHandler:@escaping ()->Void){
        manager = SocketManager(socketURL: URL(string: AppDelegate.info["ServerAddress"] as! String)!,config:[.connectParams(["token":token])])
        socket = manager.defaultSocket
        socket.on(clientEvent: .connect) {data, ack in
            completionHandler()
        }
        socket.on("error") { data, ack in
            NotificationCenter.default.post(name: .socketError, object: data)
        }
        socket.on("driverInLocation") { data, ack in
            NotificationCenter.default.post(name: .driverInLocation, object: nil)
        }
        socket.on("startTravel") { data, ack in
            NotificationCenter.default.post(name: .serviceStarted, object: nil)
        }
        socket.on("cancelTravel") { data, ack in
            NotificationCenter.default.post(name: .serviceCanceled, object: nil)
        }
        socket.on("riderInfoChanged") { data, ack in
            let rider = Rider(JSON: data[0] as! [String:Any])
            NotificationCenter.default.post(name: .riderInfoChanged, object: rider)
            AppConfig.shared.user = rider
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: AppConfig.shared)
            UserDefaults.standard.set(encodedData, forKey:"settings")
        }
        socket.on("travelInfoReceived") { data, ack in
            NotificationCenter.default.post(name: .travelInfoReceived, object: data)
        }
        socket.on("finishedTaxi") { data, ack in
            NotificationCenter.default.post(name: .serviceFinished, object: data)
        }
        socket.on("driverAccepted") {data, ack in
            let driver = Driver(JSON: data[0] as! [String:Any])
            let distance = data[1] as! Int
            let duration = data[2] as! Int
            let cost = data[3] as? Float
            let myDict: [String: Any] = ["driver": driver!, "distance": distance, "duration":duration,"cost":cost]
            NotificationCenter.default.post(name: .newDriverAccepted, object: myDict)
        }
        socket.connect()
    }
    
    func getTravels(completionHandler:@escaping (_ travels:[Travel])->Void){
        socket.emitWithAck("getTravels").timingOut(after: 15) { data in
            let travels = Mapper<Travel>().mapArray(JSONArray: data[1] as! [[String : Any]])
            completionHandler(travels)
        }
    }
    func requestTaxi(originLocation: CLLocationCoordinate2D, destinationLocation: CLLocationCoordinate2D, originAddress: String, destinationAddress: String,serviceId: Int,completionHandler:@escaping (_ result:RequestTaxiResultEvent)->Void){
        socket.emitWithAck("requestTaxi", originLocation.xy,destinationLocation.xy,originAddress,destinationAddress,serviceId).timingOut(after: 15) { data in
            let resultCode = data[0] as! Int
            var result:RequestTaxiResultEvent
            if(resultCode == 200) {
                result = RequestTaxiResultEvent(code: resultCode,driversSentTo: data[1] as! Int)
            }
            else {
                result = RequestTaxiResultEvent(code: resultCode, driversSentTo: 0)
            }
            completionHandler(result)
        }
    }
    
    func acceptDriver(driverId: Int){
        socket.emit("riderAccepted", driverId)
    }
    
    func calculateFare(pickupLocation: CLLocationCoordinate2D,destinationLocation:CLLocationCoordinate2D, completionHandler: @escaping (_ event: CalculateFareResultEvent) -> Void) {
        var pickup = [String:Any]()
        pickup["x"] = pickupLocation.longitude
        pickup["y"] = pickupLocation.latitude
        var destination = [String:Any]()
        destination["x"] = destinationLocation.longitude
        destination["y"] = destinationLocation.latitude
        socket.emitWithAck("calculateFare", pickup, destination).timingOut(after: 15) { data in
            if let code = data[0] as? Int  {
                if code == 200 {
                    ServiceCategory.lastDownloaded = Mapper<ServiceCategory>().mapArray(JSONArray: data[1] as! [[String : Any]])
                }
                completionHandler(CalculateFareResultEvent(code: code))
            }
            
        }
    }
    
    func cancelService(completionHandler:@escaping (ServerResponse)->Void){
        socket.emitWithAck("cancelTravel").timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!)
        }
    }
    
    func reviewDriver(score :Double, review:String,completionHandler:@escaping (ServerResponse)->Void){
        socket.emitWithAck("reviewDriver",score,review).timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!)
        }
    }
    
    func editProfile(profileInfo:String,completionHandler:@escaping (ServerResponse)->Void){
        socket.emitWithAck("editProfile", profileInfo).timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!)
        }
    }
    
    func changeProfileImage(imageData:Data,completionHandler:@escaping (ServerResponse,String)->Void) {
        socket.emitWithAck("changeProfileImage", imageData).timingOut(after: 15) { data in
            completionHandler(ServerResponse.init(rawValue: data[0] as! Int)!,data[1] as! String)
        }
    }
    
    func callRequest(completionHandler: @escaping (ServerResponse)->Void){
        socket.emitWithAck("callRequest").timingOut(after: 15) { data in
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!)
        }
    }
    
    func getDriversLocation(point: CLLocationCoordinate2D, completionHandler: @escaping (ServerResponse,[CLLocationCoordinate2D]) -> Void) {
        socket.emitWithAck("getDriversLocation", point.xy).timingOut(after: 15) { data in
            let json = data[1] as! [Any]
            var locations = [CLLocationCoordinate2D]()
            for location in json {
                var parsed = location as! [Any]
                let afterparsed = parsed[2] as! [String]
                locations.append(CLLocationCoordinate2D(latitude: Double(afterparsed[1])!, longitude: Double(afterparsed[0])!))
            }
            
            completionHandler(ServerResponse(rawValue: data[0] as! Int)!,locations)
        }
    }
    
    func chargeAccount(token:String, amount: Double, completionHandler: @escaping (ServerResponse,String) -> Void) {
        socket.emitWithAck("chargeAccount", "stripe", token, amount).timingOut(after: 15) { data in
            if data.count == 1 {
                completionHandler(ServerResponse(rawValue: data[0] as! Int)!,"")
            } else {
                completionHandler(ServerResponse(rawValue: data[0] as! Int)!,data[1] as! String)
            }
        }
    }
    
    func hideTravel(travelId:Int, completionHandler: @escaping (ServerResponse) -> Void) {
        socket.emitWithAck("hideTravel", travelId).timingOut(after: 15) { data in
            let result:Int
            if let resultCode = data[0] as? Int {
                result = resultCode
            } else {
                result = ServerResponse.REQUEST_TIMEOUT.rawValue
            }
            completionHandler(ServerResponse(rawValue: result)!)
        }
    }
    
    func crudAddress(crud:CRUD, address:Address,completionHandler: @escaping (ServerResponse,[Address]) -> Void) {
        socket.emitWithAck("crudAddress", crud.rawValue, address.toJSON()).timingOut(after: 15) { data in
            if crud == .READ, let jsonArray = data[1] as? [[String : Any]] {
                let addresses = Mapper<Address>().mapArray(JSONArray: jsonArray)
                completionHandler(ServerResponse(rawValue: data[0] as! Int)!,addresses)
            } else {
                completionHandler(ServerResponse(rawValue: data[0] as! Int)!,[Address]())
            }
        }
    }
}
