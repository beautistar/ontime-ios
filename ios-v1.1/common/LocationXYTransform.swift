//
//  LocationXYTransform.swift
//  rider
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

public class LocationXYTransform: TransformType {
    public typealias Object = CLLocationCoordinate2D
    public typealias JSON = [String:Double]
    
    public init() {}
    
    public func transformFromJSON(_ value: Any?) -> CLLocationCoordinate2D? {
        if let coordList = value as? [String:Double], coordList.count == 2 {
            return CLLocationCoordinate2D(latitude: coordList["y"]!, longitude: coordList["x"]!)
        }
        return nil
    }
    
    public func transformToJSON(_ value: CLLocationCoordinate2D?) -> [String:Double]? {
        if let location = value {
            var res = [String:Double]()
            res["y"] = Double(location.latitude)
            res["x"] = Double(location.longitude)
            return res
        }
        return nil
    }
}
